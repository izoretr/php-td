<form method="GET"> <!-- pas de chemin car ramène au routeur, avec un nouveau $_GET -->
    <fieldset>
        <legend style="font-size: 20px">Mon formulaire :</legend>
        <p>
            <label for="immat_id">Immatriculation :</label>
            <input type="text" placeholder="256AB34" name="immatriculation" id="immat_id"  readonly value="<?=$immat?>"/>
            <br/><br/>
            <label for="couleur_id">Couleur :</label>
            <input type="text" placeholder="Noir" name="couleur" id="couleur_id" value="<?=$couleur?>"/>
            <br/><br/>
            <label for="marque_id">Marque :</label>
            <input type="text" name="marque" id="marque_id" required value="<?=$marque?>"/>
            <br/><br/>
            <label for="placesNb_id">Nombre de places :</label>
            <input type="number" name="nbPlaces" id="placesNb_id" value="<?=$nbSieges?>"/>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
            <input type="hidden" name="controleur" value="voiture"/>
            <input type="hidden" name="action" value="modifDepuisForm"/>
        </p>
    </fieldset>
</form>
