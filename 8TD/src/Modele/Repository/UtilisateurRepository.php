<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{
    public static function construireDepuisFormulaire(array $tableauFormulaire): Utilisateur
    {
        $tableauFormulaire["emailAValider"] = $tableauFormulaire["email"];
        $tableauFormulaire["email"] = "";
        $tableauFormulaire["nonce"] = MotDePasse::genererChaineAleatoire();

        $tableauFormulaire["mdpHacheSalePoivre"] = MotDePasse::hacher($tableauFormulaire["mdpCru"]);

        $newUser = (new self())->construireDepuisTableau($tableauFormulaire);

        VerificationEmail::envoiEmailValidation($newUser);

        return $newUser;
    }

    public function construireDepuisTableau(array $objetFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $objetFormatTableau["login"],
            $objetFormatTableau["mdpHacheSalePoivre"],
            $objetFormatTableau["admin"],
            $objetFormatTableau["email"],
            $objetFormatTableau["emailAValider"],
            $objetFormatTableau["nonce"],
            $objetFormatTableau["nom"],
            $objetFormatTableau["prenom"]
        );
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'login',
            'nom',
            'prenom',
            'mdpHacheSalePoivre',
            'admin',
            'email',
            'emailAValider',
            'nonce'
        );
    }

    public static function userEstAdmin(string $login): bool
    {
        return (new self())->recupererParClePrimaire($login)->getEstAdmin();
    }
}
