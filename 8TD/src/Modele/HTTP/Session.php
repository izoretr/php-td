<?php

namespace App\Covoiturage\Modele\HTTP;

use App\Covoiturage\Configuration\ConfigurationDatabase;
use App\Covoiturage\Configuration\ConfigurationSite;
use Exception;

class Session
{
    private static ?Session $instance = null;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        if (session_start() === false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }
    }

    public static function getInstance(): Session
    {
        if (is_null(Session::$instance))
            Session::$instance = new Session();

        self::verifierDerniereActivite();
        return Session::$instance;
    }


    // ---- CRUD ----
    public function enregistrer(string $clef, mixed $valeur): void
    {
        $_SESSION[$clef] = $valeur;
    }

    public function lire(string $clef): mixed
    {
        return $_SESSION[$clef] ?? null;
    }

    public function supprimer($clef): void
    {
        unset($_SESSION[$clef]);
    }

    public function contient($clef): bool
    {
        return isset($_SESSION[$clef]);
    }

    public function detruire(): void
    {
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        Cookie::supprimer(session_name()); // deletes the session cookie
        // Il faudra reconstruire la session au prochain appel de getInstance()
        self::$instance = null;
    }

    private static function verifierDerniereActivite(): void
    {
        if (isset($_SESSION['derniereActivite']) && (time() - $_SESSION['derniereActivite'] > ConfigurationSite::dureeVieSession))
            session_unset();     // unset $_SESSION variable for the run-time

        $_SESSION['derniereActivite'] = time(); // update last activity time stamp
    }
}
