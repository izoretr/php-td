<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class VerificationEmail
{
    public static function envoiEmailValidation(Utilisateur $utilisateur): void
    {
        $loginURL = rawurlencode($utilisateur->getLogin());
        $nonceURL = rawurlencode($utilisateur->getNonce());
        $URLAbsolue = ConfigurationSite::getURLAbsolue();
        $lienValidationEmail = "$URLAbsolue?action=validerEmail&controleur=utilisateur&login=$loginURL&nonce=$nonceURL";
        $corpsEmail = "<a href=\"$lienValidationEmail\">Valider</a>";

        $to = $utilisateur->getEmailAValider();
        $subject = 'Birthday Reminders for August';
        $message = '
        <html lang="fr">
        <head>
          <title>Birthday Reminders for August</title>
        </head>
        <body>
          <p>Here are the birthdays upcoming in August!</p>
          <table>
            <tr>
              <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
            </tr>
            <tr>
              <td>Johny</td><td>10th</td><td>August</td><td>1970</td>
            </tr>
            <tr>
              <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
            </tr>
          </table>
          <p>Lien de validation : ' . $corpsEmail . '</p>
        </body>
        </html>
        ';
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        mail($to, $subject, $message, implode("\r\n", $headers));
    }

    public static function traiterEmailValidation($login, $nonce): bool
    {
        $user = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (isset($user) && $user->getNonce() == $nonce) {
            self::validerEmail($user);
            return true;
        } else {
            return false;
        }
    }

    private static function validerEmail(Utilisateur $user)
    {
        $user->setEmail($user->getEmailAValider());
        $user->setEmailAValider("");
        $user->setNonce("");
        (new UtilisateurRepository())->mettreAJour($user);
    }

    public static function aValideEmail(Utilisateur $utilisateur): bool
    {
        return $utilisateur->getEmail() != "";
    }
}
