<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\VoitureRepository as VoitureRepository;

class ControleurVoiture
{
    public static function afficherListe(): void
    {
        $voitures = (new VoitureRepository())->recuperer(); //appel au modèle pour gerer la BD
        ControleurGenerique::afficherVueDansCorps('Liste de voitures', 'voiture/liste.php', [
            'voitures' => $voitures
        ]);
    }
    public static function afficherDetail(): void
    {
        $immat = $_GET['immatriculation'];
        $voiture = (new VoitureRepository())->recupererParClePrimaire($immat);

        if (isset($voiture))
            ControleurGenerique::afficherVueDansCorps("Details", 'voiture/detail.php', [
                'voiture' => $voiture
            ]);
        else
            self::afficherErreur("Voiture non trouvée");
    }
    public static function afficherFormulaireCreation(): void
    {
        ControleurGenerique::afficherVueDansCorps("Formulaire de création", 'voiture/formulaireCreation.html');
    }
    public static function afficherFormulaireMiseAJour() : void {
        $immat = $_GET['immatriculation'];
        $voiture = (new VoitureRepository())->recupererParClePrimaire($immat);

        if (isset($voiture))
            ControleurGenerique::afficherVueDansCorps("MàJ voiture", 'voiture/formulaireMiseAJour.php', [
                'immat' => $voiture->getImmatriculation(),
                'couleur' => $voiture->getCouleur(),
                'marque' => $voiture->getMarque(),
                'nbSieges' => $voiture->getNbSieges()
            ]);
        else
            self::afficherErreur("Voiture non trouvée");
    }
    public static function afficherErreur(string $error) : void {
        ControleurGenerique::afficherVueDansCorps("ERREUR", 'voiture/erreur.php', [
            'errorstr' => $error
        ]);
    }

    // ----- CRUD -----
    public static function creerDepuisFormulaire(): void
    {
        $tableauVoiture = [
            'immatriculation' => $_GET['immatriculation'],
            'couleur' => $_GET['couleur'],
            'marque' => $_GET['marque'],
            'nbSieges' => $_GET['nbPlaces']
        ];

        $voiture = (new VoitureRepository())->construireDepuisTableau($tableauVoiture);

        (new VoitureRepository())->sauvegarder($voiture);

        ControleurGenerique::afficherVueDansCorps("Liste", 'voiture/voitureCreee.php', [
            'voitures' => (new VoitureRepository())->recuperer()
        ]);
    }
    public static function modifDepuisForm() : void {
        $tableauVoiture = [
            'immatriculation' => $_GET['immatriculation'],
            'couleur' => $_GET['couleur'],
            'marque' => $_GET['marque'],
            'nbSieges' => $_GET['nbPlaces']
        ];

        $voiture = (new VoitureRepository())->construireDepuisTableau($tableauVoiture);

        (new VoitureRepository())->mettreAJour($voiture);

        ControleurGenerique::afficherVueDansCorps("Liste", 'voiture/voitureMiseAJour.php', [
            'immatriculation' => $voiture->getImmatriculation(),
            'voitures' => (new VoitureRepository())->recuperer()
        ]);
    }
    public static function supprimer() : void {
        if (isset($_GET['immatriculation'])) {
            $immat = $_GET['immatriculation'];
            (new VoitureRepository())->supprimer($immat);
            ControleurGenerique::afficherVueDansCorps("Liste", 'voiture/voitureSupprimee.php', ['immatriculation' => $immat, 'voitures' => (new VoitureRepository())->recuperer()]);
        } else
            self::afficherErreur("Immatriculation à supprimer non-précisée");
    }
}
