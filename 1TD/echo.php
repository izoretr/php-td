<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
    <body>
        <p>Voici le résultat du script PHP :</p>

        <?php
        echo "<p>";

        $texte = "hello world !";
        echo $texte;
        $prenom = "Marc";

        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";

        echo "</p>";

        /*$voiture = [
            'marque' => "MonolithSoft",
            'couleur' => "NOIR",
            'immatriculation' => "4200000",
            'nbSieges' => 5,
        ];*/
        $voitures = [[
                'marque' => "MonolithSoft",
                'couleur' => "NOIR",
                'immatriculation' => "4200000",
                'nbSieges' => 5,
            ], [
                'marque' => "Reno",
                'couleur' => "beige",
                'immatriculation' => "AZ",
                'nbSieges' => 3,
            ], [
                'marque' => "Moto34",
                'couleur' => "Rougeee",
                'immatriculation' => "segmentation fault (core dumped)",
                'nbSieges' => 1,
            ],
        ];

        echo "<h1>Voitures :</h1>";

        if (empty($voitures)) echo "Il n’y a aucune voiture.";
        else {
            echo "<ul>";
            foreach ($voitures as $car) {
                echo "<li> Voiture $car[immatriculation] de marque $car[marque] (couleur $car[couleur], $car[nbSieges] sieges) </li>";
            }
            echo "</ul>";
        };

        ?>
    </body>
</html>


