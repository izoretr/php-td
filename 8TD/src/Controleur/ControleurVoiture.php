<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\VoitureRepository as VoitureRepository;

class ControleurVoiture extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $voitures = (new VoitureRepository())->recuperer(); //appel au modèle pour gerer la BD
        self::afficherVueDansCorps('Liste de voitures', 'voiture/liste.php', [
            'voitures' => $voitures
        ]);
    }
    public static function afficherDetail(): void
    {
        $immat = $_GET['immatriculation'];
        $voiture = (new VoitureRepository())->recupererParClePrimaire($immat);

        if (isset($voiture))
            self::afficherVueDansCorps("Details", 'voiture/detail.php', [
                'voiture' => $voiture
            ]);
        else
            self::afficherErreur("Voiture non trouvée");
    }
    public static function afficherFormulaireCreation(): void
    {
        self::afficherVueDansCorps("Formulaire de création", 'voiture/formulaireCreation.php');
    }
    public static function afficherFormulaireMiseAJour() : void {
        $immat = $_GET['immatriculation'];
        $voiture = (new VoitureRepository())->recupererParClePrimaire($immat);

        if (isset($voiture))
            self::afficherVueDansCorps("MàJ voiture", 'voiture/formulaireMiseAJour.php', [
                'immat' => $voiture->getImmatriculation(),
                'couleur' => $voiture->getCouleur(),
                'marque' => $voiture->getMarque(),
                'nbSieges' => $voiture->getNbSieges()
            ]);
        else
            self::afficherErreur("Voiture non trouvée");
    }

    // ----- CRUD -----
    public static function creerDepuisFormulaire(): void
    {
        $tableauVoiture = [
            'immatriculation' => $_GET['immatriculation'],
            'couleur' => $_GET['couleur'],
            'marque' => $_GET['marque'],
            'nbSieges' => $_GET['nbPlaces']
        ];

        $voiture = (new VoitureRepository())->construireDepuisTableau($tableauVoiture);

        (new VoitureRepository())->sauvegarder($voiture);

        self::afficherVueDansCorps("Liste", 'voiture/voitureCreee.php', [
            'voitures' => (new VoitureRepository())->recuperer()
        ]);
    }
    public static function modifDepuisForm() : void {
        $tableauVoiture = [
            'immatriculation' => $_GET['immatriculation'],
            'couleur' => $_GET['couleur'],
            'marque' => $_GET['marque'],
            'nbSieges' => $_GET['nbPlaces']
        ];

        $voiture = (new VoitureRepository())->construireDepuisTableau($tableauVoiture);

        (new VoitureRepository())->mettreAJour($voiture);

        self::afficherVueDansCorps("Liste", 'voiture/voitureMiseAJour.php', [
            'immatriculation' => $voiture->getImmatriculation(),
            'voitures' => (new VoitureRepository())->recuperer()
        ]);
    }
    public static function supprimer() : void {
        if (isset($_GET['immatriculation'])) {
            $immat = $_GET['immatriculation'];
            (new VoitureRepository())->supprimer($immat);
            self::afficherVueDansCorps("Liste", 'voiture/voitureSupprimee.php', ['immatriculation' => $immat, 'voitures' => (new VoitureRepository())->recuperer()]);
        } else
            self::afficherErreur("Immatriculation à supprimer non-précisée");
    }
}
