<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
$loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Controleur\ControleurVoiture;

if (key_exists('action', $_GET)) {
    $action = $_GET['action'];
    ControleurVoiture::$action();
}
