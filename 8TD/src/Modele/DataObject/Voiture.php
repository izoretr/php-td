<?php

namespace App\Covoiturage\Modele\DataObject;

class Voiture extends AbstractDataObject
{
    private string $immatriculation;
    private string $marque;
    private string $couleur;
    private int $nbSieges;

    public function __construct(string $immatriculation, string $marque, string $couleur, int $nbSieges)
    {
        $this->immatriculation = substr($immatriculation, 0, 8);
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->nbSieges = $nbSieges;
    }

    public function formatTableau() : array
    {
        return array(
            "immatriculationTag" => $this->immatriculation,
            "marqueTag" => $this->marque,
            "couleurTag" => $this->couleur,
            "nbSiegesTag" => $this->nbSieges
        );
    }

    public function getMarque(): string
    {
        return $this->marque;
    }

    public function getImmatriculation(): string
    {
        return $this->immatriculation;
    }

    public function getNbSieges(): int
    {
        return $this->nbSieges;
    }

    public function getCouleur(): string
    {
        return $this->couleur;
    }

    public function setMarque(string $marque): void
    {
        $this->marque = $marque;
    }

    public function setImmatriculation(string $immatriculation): void
    {
        $this->immatriculation = substr($immatriculation, 0, 8);
    }

    public function setCouleur(string $couleur): void
    {
        $this->couleur = $couleur;
    }

    public function setNbSieges(int $nbSieges): void
    {
        $this->nbSieges = $nbSieges;
    }
}
