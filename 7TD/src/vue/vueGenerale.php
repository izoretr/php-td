<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>
    <link rel="stylesheet" href=<?= "../ressources/css/mainStyle.css" ?> />
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=voiture">Gestion des voitures</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=formulairePreference&controleur=generique"><img src="../ressources/img/heart.png" alt="heartIcon" /></a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/$cheminVueBody";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage CovoitureACar - Tous droits réservés
    </p>
</footer>
</body>
</html>
