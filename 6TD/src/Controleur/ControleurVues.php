<?php

namespace App\Covoiturage\Controleur;

class ControleurVues
{
    public static function afficherVueCrue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue";
    }
    public static function afficherVueDansCorps(string $titrePage, string $cheminVue, array $parametres = []): void
    {
        self::afficherVueCrue("vueGenerale.php", array_merge(
            [
                'pageTitle' => $titrePage,
                'cheminVueBody' => $cheminVue
            ],
            $parametres
        ));
    }
}
