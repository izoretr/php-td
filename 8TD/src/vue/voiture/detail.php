<p>La voiture
    <?= htmlspecialchars($voiture->getImmatriculation()); ?> a pour marque
    <?= htmlspecialchars($voiture->getMarque()); ?>.<br/>Elle est de couleur
    <?= htmlspecialchars($voiture->getCouleur()); ?>, et possède
    <?= htmlspecialchars($voiture->getNbSieges()); ?> sieges.
    <br/>
    <a href="?controleur=voiture&action=supprimer&immatriculation=<?=rawurlencode($voiture->getImmatriculation())?>">Supprimer</a>
    <a href="?controleur=voiture&action=afficherFormulaireMiseAJour&immatriculation=<?=rawurlencode($voiture->getImmatriculation())?>">MàJ</a>
</p>
