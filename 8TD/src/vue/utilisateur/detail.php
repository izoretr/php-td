<p>L'utilisateur
    <?= htmlspecialchars($user->getLogin()); ?> se nomme
    <?= htmlspecialchars($user->getPrenom()); ?>
    <?= htmlspecialchars($user->getNom()); ?>.
    <br/>
    <?php
    if ($estProprioDuCompte) {
        echo '<a href="?controleur=utilisateur&action=supprimer&login=' . rawurlencode($user->getLogin()) . '">Supprimer</a>';
        echo '<a href="?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . rawurlencode($user->getLogin()) . '">MàJ</a>';
    }
    ?>
</p>
