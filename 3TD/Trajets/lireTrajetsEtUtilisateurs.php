<?php
    require_once "Trajet.php";
    require_once "Utilisateur.php";

    echo '<ul>Trajets :';
    foreach (Trajet::getTrajets() as $trajet) {
        echo "<li>$trajet</li>";
    }
    echo '</ul><br/>';

    echo '<ul>Utilisateurs :';
    foreach (Utilisateur::getUtilisateurs() as $user) {
        echo "<li>$user</li>";
    }
    echo '</ul><br/>';
