<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    public const dureeVieSession = 3600;

    public static function getURLAbsolue() {
        return "https://webinfo.iutmontp.univ-montp2.fr/~izoretr/PHP-TD/8TD/web/controleurFrontal.php";
    }
}