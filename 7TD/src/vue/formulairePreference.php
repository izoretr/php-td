<form method="GET">
    <input type="radio" id="voitureId" name="controleur_defaut" value="voiture" <?php if ($current == "voiture") echo "checked"; ?>/>
    <label for="voitureId">Voiture</label>
    <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php if ($current == "utilisateur") echo "checked"; ?>/>
    <label for="utilisateurId">Utilisateur</label>
    <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php if ($current == "trajet") echo "checked"; ?>/>
    <label for="trajetId">Trajet</label>

    <input type="hidden" name="controleur" value="generique"/>
    <input type="hidden" name="action" value="enregistrerPreference"/>

    <input type="submit" value="Enregistrer"/>
</form>
