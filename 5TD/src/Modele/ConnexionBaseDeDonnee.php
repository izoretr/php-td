<?php
namespace App\Covoiturage\Modele;

use App\Covoiturage\Configuration\ConfigurationDatabase as Conf;
use PDO;

class ConnexionBaseDeDonnee
{
    private static ?ConnexionBaseDeDonnee $instance = null;

    private PDO $pdo;

    private function __construct()
    {
        $hostname = Conf::getHostname();
        $databaseName = Conf::getDatabase();
        $port = Conf::getPort();
        $login = Conf::getLogin();
        $password = Conf::getPassword();

        // Connexion à la base de données
        // Le dernier argument sert à ce que toutes les chaines de caractères
        // en entrée et sortie de MySql soit dans le codage UTF-8
        $this->pdo = new PDO("mysql:host=$hostname;port=$port;dbname=$databaseName", $login, $password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

        // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public static function getPdo(): PDO
    {
        return ConnexionBaseDeDonnee::getInstance()->pdo;
    }

    public static function getInstance(): ConnexionBaseDeDonnee
    {
        if (is_null(ConnexionBaseDeDonnee::$instance))
            ConnexionBaseDeDonnee::$instance = new ConnexionBaseDeDonnee();
        return ConnexionBaseDeDonnee::$instance;
    }

}
