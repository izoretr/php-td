<?php
    require_once "Conf.php";

    class Model {
        private static ?Model $instance = null;

        private PDO $pdo;

        private function __construct() {
            $hostname = Conf::getHostname();
            $databaseName = Conf::getDatabase();
            $port = Conf::getPort();
            $login = Conf::getLogin();
            $password = Conf::getPassword();

            // Connexion à la base de données
            // Le dernier argument sert à ce que toutes les chaines de caractères
            // en entrée et sortie de MySql soit dans le codage UTF-8
            $this->pdo = new PDO("mysql:host=$hostname;port=$port;dbname=$databaseName", $login, $password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

            // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        public static function getPdo(): PDO {
            return Model::getInstance()->pdo;
        }

        public static function getInstance() : Model {
            if (is_null(Model::$instance))
                Model::$instance = new Model();
            return Model::$instance;
        }

    }
