<form method="GET"> <!-- pas de chemin car ramène au routeur, avec un nouveau $_GET -->
    <fieldset>
        <legend style="font-size: 20px">Modifications :</legend>
        <p>
            <label for="login_id">Login&#42; :</label>
            <input type="text" name="login" id="login_id" readonly value="<?= $login ?>"/>
            <br/><br/>
            <label for="prenom_id">Prenom :</label>
            <input type="text" placeholder="Jean" name="prenom" id="prenom_id" value="<?= $prenom ?>"/>
            <br/><br/>
            <label for="nom_id">Nom :</label>
            <input type="text" placeholder="Hubert" name="nom" id="nom_id" value="<?= $nom ?>"/>
        </p>
        <p class="InputAddOn">
            <?php if (\App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur())
                echo '<label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin"
                   id="estAdmin_id" ' . (\App\Covoiturage\Modele\Repository\UtilisateurRepository::userEstAdmin($login) ? 'checked' : '') . '>
            <br/><br/>'; ?>
            <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe&#42; :</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
            <br/><br/>
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau mot de passe&#42; :</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
            <br/><br/>
            <label class="InputAddOn-item" for="current_mdp_id">Mot de passe actuel&#42; :</label>
            <input class="InputAddOn-field" type="password" name="current_mdp" id="current_mdp_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
            <input type="hidden" name="controleur" value="utilisateur"/>
            <input type="hidden" name="action" value="modifDepuisForm"/>
        </p>
    </fieldset>
</form>
