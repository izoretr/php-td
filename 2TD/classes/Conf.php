<?php
class Conf {
    static private array $databaseConfiguration = array(
        // Le nom d'hote est webinfo a l'IUT
        // ou localhost sur votre machine
        // ou webinfo.iutmontp.univ-montp2.fr pour accéder à webinfo depuis l'extérieur
        'hostname' => 'localhost',
        // A l'IUT, vous avez une BDD nommee comme votre login
        // Sur votre machine, vous devrez creer une BDD
        'database' => 'phptd',
        // À l'IUT, le port de MySQL est particulier : 3316
        // Ailleurs, on utilise le port par défaut : 3306
        'port' => '3306',
        // A l'IUT, c'est votre login
        // Sur votre machine, vous avez surement un compte 'root'
        'login' => 'notisma',
        // A l'IUT, c'est votre mdp (INE par defaut)
        // Sur votre machine personelle, vous avez créé ce mdp a l'installation
        'password' => 'final'
    );

    public static function getHostname() : string {
        return Conf::$databaseConfiguration['hostname'];
    }
    public static function getDatabase() : string {
        return Conf::$databaseConfiguration['database'];
    }
    public static function getPort() : string {
        return Conf::$databaseConfiguration['port'];
    }
    static public function getLogin() : string {
        return Conf::$databaseConfiguration['login'];
    }
    public static function getPassword() : string {
        return Conf::$databaseConfiguration['password'];
    }
}
