<p>La voiture
    <?= htmlspecialchars($voiture->getImmatriculation()); ?> a pour marque
    <?= htmlspecialchars($voiture->getMarque()); ?>.<br/>Elle est de couleur
    <?= htmlspecialchars($voiture->getCouleur()); ?>, et possède
    <?= htmlspecialchars($voiture->getNbSieges()); ?> sieges.
</p>
