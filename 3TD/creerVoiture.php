<?php
    require_once "classes/Voiture.php";
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Créer Voiture</title>
    </head>
    <body>
        <?php
            if (empty($_POST))
                echo "<p>Erreur 69 : Cette page doit être accédée à partir du formulaire.</p>";
            else {
                echo '<p>';

                $carSent = new Voiture($_POST['immatriculation'], $_POST['brand'], $_POST['color'], $_POST['seatsNumber']);

                echo "Voiture input : $carSent.";

                $carSent->sauvegarder();

                echo '</p>';
            }
        ?>
    </body>
</html>
