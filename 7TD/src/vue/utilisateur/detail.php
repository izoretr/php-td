<p>L'utilisateur
    <?= htmlspecialchars($user->getLogin()); ?> se nomme
    <?= htmlspecialchars($user->getPrenom()); ?>
    <?= htmlspecialchars($user->getNom()); ?>.
    <br/>
    <a href="?controleur=utilisateur&action=supprimer&login=<?=rawurlencode($user->getLogin())?>">Supprimer</a>
    <a href="?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=<?=rawurlencode($user->getLogin())?>">MàJ</a>
</p>
