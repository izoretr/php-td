<?php
require_once "ConnexionBaseDeDonnee.php";
class ModeleVoiture {
    private string $immatriculation;
    private string $marque;
    private string $couleur;
    private int $nbSieges;

    public function __construct(string $immatriculation, string $marque, string $couleur, int $nbSieges){
        $this->immatriculation = substr($immatriculation, 0, 8);
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->nbSieges = $nbSieges;
    }

    public static function construireDepuisTableau(array $voitureFormatTableau) : ModeleVoiture {
        return new ModeleVoiture($voitureFormatTableau['immatriculation'], $voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['nbSieges']);
    }
    public static function getVoitures() : array {
        $pdo = ConnexionBaseDeDonnee::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM voiture;");
        $pdoStatement->setFetchMode(PDO::FETCH_ASSOC);

        $voitures = array();
        foreach ($pdoStatement as $voitureDB) {
            $voitures[] = self::construireDepuisTableau($voitureDB);
        }
        return $voitures;
    }
    public static function getVoitureParImmat(string $immatriculation) : ?ModeleVoiture {
        $sql = "SELECT * from voiture WHERE immatriculation = :immatriculationTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voitureData = $pdoStatement->fetch();

        if ($voitureData)
            return ModeleVoiture::construireDepuisTableau($voitureData);
        else
            return null;
    }

    public function sauvegarder() : void {
        $query = "INSERT IGNORE INTO voiture VALUES (:imm, :marq, :col, :sieg);";
        $prepared = ConnexionBaseDeDonnee::getPdo()->prepare($query);

        // éviter full paté comme ça
        $prepared->execute(array('imm' => $this->immatriculation, 'marq' => $this->marque, 'col' => $this->couleur, 'sieg' => $this->nbSieges));
    }
/*
    public function __toString() : string {
        return "Voiture $this->immatriculation de marque $this->marque (couleur $this->couleur, $this->nbSieges sieges)";
    }
*/
    public function getMarque() : string {
        return $this->marque;
    }
    public function getImmatriculation(): string {
        return $this->immatriculation;
    }
    public function getNbSieges() : int {
        return $this->nbSieges;
    }
    public function getCouleur() : string {
        return $this->couleur;
    }
    public function setMarque(string $marque): void {
        $this->marque = $marque;
    }
    public function setImmatriculation(string $immatriculation): void {
        $this->immatriculation = substr($immatriculation, 0, 8);
    }
    public function setCouleur(string $couleur): void {
        $this->couleur = $couleur;
    }
    public function setNbSieges(int $nbSieges): void {
        $this->nbSieges = $nbSieges;
    }

}
