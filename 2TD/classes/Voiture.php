<?php
class Voiture {

    private string $immatriculation;
    private string $marque;
    private string $couleur;
    private int $nbSieges;

    public function __construct(string $immatriculation, string $marque, string $couleur, int $nbSieges){
        $this->immatriculation = substr($immatriculation, 0, 8);
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->nbSieges = $nbSieges;
    }

    public function getMarque() : string {
        return $this->marque;
    }
    public function getImmatriculation(): string {
        return $this->immatriculation;
    }
    public function getNbSieges() : int {
        return $this->nbSieges;
    }
    public function getCouleur() : string {
        return $this->couleur;
    }
    public function setMarque(string $marque): void {
        $this->marque = $marque;
    }
    public function setImmatriculation(string $immatriculation): void {
        $this->immatriculation = substr($immatriculation, 0, 8);
    }
    public function setCouleur(string $couleur): void {
        $this->couleur = $couleur;
    }
    public function setNbSieges(int $nbSieges): void {
        $this->nbSieges = $nbSieges;
    }

    public static function construireDepuisTableau(array $voitureFormatTableau) : Voiture {
        return new Voiture($voitureFormatTableau['immatriculation'], $voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['nbSieges']);
    }
    public static function getVoitures() : array {
        $pdo = Model::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM voiture;");
        $pdoStatement->setFetchMode(PDO::FETCH_ASSOC);

        $voitures = array();
        foreach ($pdoStatement as $voitureDB) {
            $voitures[] = self::construireDepuisTableau($voitureDB);
        }
        return $voitures;
    }

    public function __toString() : string {
        return "Voiture $this->immatriculation de marque $this->marque (couleur $this->couleur, $this->nbSieges sieges)";
    }
}
