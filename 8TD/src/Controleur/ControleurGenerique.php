<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVueCrue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue";
    }

    protected static function afficherVueDansCorps(string $titrePage, string $cheminVue, array $parametres = []): void
    {
        self::afficherVueCrue("vueGenerale.php", array_merge(
            [
                'pageTitle' => $titrePage,
                'cheminVueBody' => $cheminVue
            ],
            $parametres
        ));
    }

    public static function afficherErreur(string $error): void
    {
        self::afficherVueDansCorps("ERREUR", 'erreur.php', [
            'errorstr' => $error
        ]);
    }

    public static function formulairePreference(): void
    {
        self::afficherVueDansCorps("Préférences", 'formulairePreference.php', [
            'current' => PreferenceControleur::lire()
        ]);
    }

    public static function enregistrerPreference(): void
    {
        $controleurPref = $_GET['controleur_defaut'];
        PreferenceControleur::enregistrer($controleurPref);
        self::afficherVueDansCorps("Pref enregistrée", "enregistrerPreference.php");
    }
}
