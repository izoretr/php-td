<?php

namespace App\Covoiturage\Configuration;

class ConfigurationDatabase
{

    /*
    // Maison
    static private array $databaseConfiguration = array(
        // Le nom d'hote est webinfo a l'IUT
        // ou localhost sur votre machine
        // ou webinfo.iutmontp.univ-montp2.fr pour accéder à webinfo depuis l'extérieur
        'hostname' => 'localhost',
        // A l'IUT, vous avez une BDD nommee comme votre login
        // Sur votre machine, vous devrez creer une BDD
        'database' => 'phptd',
        // À l'IUT, le port de MySQL est particulier : 3316
        // Ailleurs, on utilise le port par défaut : 3306
        'port' => '3306',
        // A l'IUT, c'est votre login
        // Sur votre machine, vous avez surement un compte 'root'
        'login' => 'notisma',
        // A l'IUT, c'est votre mdp (INE par defaut)
        // Sur votre machine personelle, vous avez créé ce mdp a l'installation
        'password' => 'final'
    );
    */
    // IUT
    static private array $databaseConfiguration = array(
        // webinfo à l'IUT
        // localhost sur votre machine
        // webinfo.iutmontp.univ-montp2.fr pour accéder à webinfo depuis l'extérieur
        'hostname' => 'webinfo',
        // A l'IUT, vous avez une BDD nommee comme votre login
        // Sur votre machine, vous devrez creer une BDD
        'database' => 'izoretr',
        // À l'IUT, le port de MySQL est particulier : 3316
        // Ailleurs, on utilise le port par défaut : 3306
        'port' => '3316',
        // A l'IUT, c'est votre login
        // Sur votre machine, vous avez surement un compte 'root'
        'login' => 'izoretr',
        // A l'IUT, c'est votre mdp (INE par defaut)
        // Sur votre machine personelle, vous avez créé ce mdp a l'installation
        'password' => ''
    );


    public static function getHostname(): string
    {
        return ConfigurationDatabase::$databaseConfiguration['hostname'];
    }

    public static function getDatabase(): string
    {
        return ConfigurationDatabase::$databaseConfiguration['database'];
    }

    public static function getPort(): string
    {
        return ConfigurationDatabase::$databaseConfiguration['port'];
    }

    static public function getLogin(): string
    {
        return ConfigurationDatabase::$databaseConfiguration['login'];
    }

    public static function getPassword(): string
    {
        return ConfigurationDatabase::$databaseConfiguration['password'];
    }
}
