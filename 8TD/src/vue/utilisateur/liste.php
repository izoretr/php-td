<h2>Utilisateurs (<a href="?controleur=utilisateur&action=afficherFormulaireCreation">ajouter</a>) : </h2>
<ul>
    <?php
    foreach ($utilisateurs as $user) {
        $login = $user->getLogin();

        echo '<li> Utilisateur <a href="?controleur=utilisateur&action=afficherDetail&login='
            , rawurlencode($login) . '">'
            , htmlspecialchars($login)
            , '</a></li>'
        ;
    }
    ?>
</ul>
