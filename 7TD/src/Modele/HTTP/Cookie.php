<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = 0): void
    {
        $valeurSrialized = serialize($valeur);
        setcookie($cle, $valeurSrialized, $dureeExpiration);
    }

    public static function lire(string $cle): mixed
    {
        if (isset($_COOKIE[$cle])) {
            $strCook = $_COOKIE[$cle];
            return unserialize($strCook);
        } else {
            return false;
        }
    }

    public static function contient($cle): bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle): void
    {
        unset($_COOKIE[$cle]);
        setcookie($cle, "", 1);
    }
}