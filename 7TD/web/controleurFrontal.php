<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
$loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Controleur\ControleurGenerique;
use App\Covoiturage\Lib\PreferenceControleur;

if (key_exists('controleur', $_GET))
    $controleur = $_GET['controleur'];
elseif (PreferenceControleur::existe())
    $controleur = PreferenceControleur::lire();
else
    $controleur = "voiture";

$nomClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);

if (class_exists($nomClasseControleur)) {
    if (key_exists('action', $_GET))
        $action = $_GET['action'];
    else
        $action = "afficherListe";

    if (in_array($action, get_class_methods($nomClasseControleur))) {
        $nomClasseControleur::$action();
    }
    else
        $nomClasseControleur::afficherErreur("Action invalide");
} else {
    ControleurGenerique::afficherErreur("controleur inexistant");
}
