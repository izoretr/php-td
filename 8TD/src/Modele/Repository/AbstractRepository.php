<?php

namespace App\Covoiturage\Modele\Repository;

use PDO;
use App\Covoiturage\Modele\DataObject\AbstractDataObject as AbstractDataObject;

abstract class AbstractRepository
{
    protected abstract function getNomTable() : string;
    protected abstract function getNomClePrimaire() : string;
    protected abstract function getNomsColonnes() : array;
    protected abstract function construireDepuisTableau(array $objetFormatTableau) : AbstractDataObject;

    public function recuperer() : array {
        $table = $this->getNomTable();

        $pdo = ConnexionBaseDeDonnee::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM $table;");
        $pdoStatement->setFetchMode(PDO::FETCH_ASSOC);

        $objets = array();
        foreach ($pdoStatement as $objetDB) {
            $objets[] = $this->construireDepuisTableau($objetDB);
        }

        return $objets;
    }
    public function recupererParClePrimaire(string $valeurClePrimaire): ?AbstractDataObject {
        $table = $this->getNomTable();
        $clePrim = $this->getNomClePrimaire();

        $sql = "SELECT * from $table WHERE $clePrim = :prim";

        $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);

        $values = array(
            "prim" => $valeurClePrimaire,
        );

        $pdoStatement->execute($values);

        // fetch() renvoie false si trouve rien
        $objectData = $pdoStatement->fetch();

        if ($objectData)
            return $this->construireDepuisTableau($objectData);
        else
            return null;
    }

    // ---------- CRUD ------------
    public function sauvegarder(AbstractDataObject $obj) : void {
        $table = $this->getNomTable();
        $clePrim = $this->getNomClePrimaire();
        $colonnes = $this->getNomsColonnes();

        $query = "";
        $firstTurn = true;
        foreach ($colonnes as $col) { // on rajoute tous les arguments modifiables à la query
            if ($firstTurn) {
                $query = "INSERT IGNORE INTO $table VALUES (:$col" . "Tag"; // si premier tour, pas de ", " devant la colonne
                $firstTurn = false;
            } else {
                $query .= ", :$col" . "Tag";
            }
        }
        if ($query != "") { // si on a bien fait le for
            $query .= ");";

            $prepared = ConnexionBaseDeDonnee::getPdo()->prepare($query);

            $tags = $obj->formatTableau();

            $prepared->execute($tags);
        }
    }
    public function mettreAJour(AbstractDataObject $object) : void {
        $table = $this->getNomTable();
        $clePrim = $this->getNomClePrimaire();
        $colonnes = $this->getNomsColonnes();

        $query = "";
        $firstTurn = true;
        foreach ($colonnes as $col) { // on rajoute tous les arguments modifiables à la query
            if ($firstTurn) {
                $query = "UPDATE $table SET $col = :" . $col . "Tag"; // si premier tour, pas de ", " devant la colonne
                $firstTurn = false;
            } else {
                $query .= ", $col = :$col" . "Tag";
            }
        }
        if ($query != "") { // si on a bien fait le for
            $query .= " WHERE $clePrim = :$clePrim" . "Tag;";

            $prepared = ConnexionBaseDeDonnee::getPdo()->prepare($query);

            $tags = $object->formatTableau();

            $prepared->execute($tags);
        }
    }
    public function supprimer(string $valeurClePrimaire) : void {
        $table = $this->getNomTable();
        $clePrim = $this->getNomClePrimaire();

        ConnexionBaseDeDonnee::getPdo()->prepare("DELETE FROM $table WHERE $clePrim = :p;")->execute(['p' => $valeurClePrimaire]);
    }
}
