<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session, associé à la clef suivante :
    private static string $clefConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        Session::getInstance()->enregistrer(self::$clefConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        //echo (isset($_SESSION[self::$cleConnexion])) ? $_SESSION[self::$clefConnexion] : "pas co";
        return Session::getInstance()->contient(self::$clefConnexion);
    }

    public static function deconnecter(): void
    {
        Session::getInstance()->supprimer(self::$clefConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return Session::getInstance()->lire(self::$clefConnexion) ?? null;
    }

    public static function estUtilisateur(string $login): bool
    {
        return self::getLoginUtilisateurConnecte() === $login;
    }

    public static function estAdministrateur(): bool
    {
        if (self::estConnecte())
            return (new UtilisateurRepository())->recupererParClePrimaire(self::getLoginUtilisateurConnecte())->getEstAdmin();
        else
            return false;
    }
}
