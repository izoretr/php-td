<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository as UtilisateurRepository;

//use App\Covoiturage\Modele\HTTP\Cookie as Cook;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherListe(): void
    {
        $users = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gerer la BD
        self::afficherVueDansCorps('Liste d\'utilisateurs', 'utilisateur/liste.php', [
            'utilisateurs' => $users
        ]);
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $estBonUser = ConnexionUtilisateur::estUtilisateur($login);
        } else {
            $login = ConnexionUtilisateur::getLoginUtilisateurConnecte();
            $estBonUser = true;
        }

        $user = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (isset($user))
            self::afficherVueDansCorps("Details", 'utilisateur/detail.php', [
                'user' => $user,
                'estProprioDuCompte' => ($estBonUser || ConnexionUtilisateur::estAdministrateur())
            ]);
        else
            self::afficherErreur("Utilisateur non trouvé");
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        if (isset($_GET['login']))
            $login = $_GET['login'];
        else {
            self::afficherErreur("Pas de login trouvé");
            return;
        }
        if (!(ConnexionUtilisateur::estUtilisateur($login) || ConnexionUtilisateur::estAdministrateur())) {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (isset($utilisateur))
            self::afficherVueDansCorps("MàJ utilisateur", 'utilisateur/formulaireMiseAJour.php', [
                'login' => $utilisateur->getLogin(),
                'prenom' => $utilisateur->getPrenom(),
                'nom' => $utilisateur->getNom()
            ]);
        else
            self::afficherErreur("Utilisateur non trouvé");
    }

// ----- CONNEXION -----

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVueDansCorps("Connexion", "utilisateur/formulaireConnexion.php");
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVueDansCorps("Formulaire de création", 'utilisateur/formulaireCreation.php');
    }

    public static function connecter(): void
    {
        if (!isset($_GET['login']) || !isset($_GET['password']))
            self::afficherErreur("Il manque le login ou le mot de passe...");
        else {
            $user = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if (!$user)
                self::afficherErreur("Utilisateur inconnu :(");
            else if (!MotDePasse::verifier($_GET['password'], $user->getMdpHache()))
                self::afficherErreur("Mot de passe erroné");
            else if (!VerificationEmail::aValideEmail($user)) {
                self::afficherErreur("Email non validé yet");
            }
            else {
                ConnexionUtilisateur::connecter($user->getLogin());
                self::afficherVueDansCorps("Connexion réussie !", "utilisateur/utilisateurConnecte.php", [
                    "prenom" => $user->getPrenom(),
                    "nom" => $user->getNom(),
                    'utilisateurs' => (new UtilisateurRepository())->recuperer()
                ]);
            }
        }
    }

    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        self::afficherVueDansCorps("Déconnecté", "utilisateur/utilisateurDeconnecte.php", ['utilisateurs' => (new UtilisateurRepository())->recuperer()]);
    }


// ----- CRUD -----

    public static function creerDepuisFormulaire(): void
    {
        $mdp = $_GET['mdp'];
        $mdpVerif = $_GET['mdp2'];

        if ($mdp != $mdpVerif) {
            self::afficherErreur("Les mots de passe sont distincts !");
        } else {
            $tableauUtilisateur = [
                'login' => $_GET['login'],
                'nom' => $_GET['nom'],
                'prenom' => $_GET['prenom'],
                'mdpCru' => $mdp,
                'admin' => (isset($_GET['estAdmin']) && $_GET['estAdmin'] == 'on' && ConnexionUtilisateur::estAdministrateur()),
                'email' => $_GET['email']
            ];

            $user = UtilisateurRepository::construireDepuisFormulaire($tableauUtilisateur);

            (new UtilisateurRepository())->sauvegarder($user);

            self::afficherVueDansCorps("Liste", 'utilisateur/utilisateurCree.php', [
                'utilisateurs' => (new UtilisateurRepository())->recuperer()
            ]);
        }
    }

    public static function modifDepuisForm(): void
    {
        if (isset($_GET['login']))
            $login = $_GET['login'];
        else {
            self::afficherErreur("Pas de login trouvé");
            return;
        }
        if (!(ConnexionUtilisateur::estUtilisateur($login) || ConnexionUtilisateur::estAdministrateur())) {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            return;
        }
        if (!isset($_GET['current_mdp']) || !isset($_GET['mdp']) || !isset($_GET['mdp2'])) {
            self::afficherErreur("Manque de données.");
            return;
        }

        $currentMdp = $_GET['current_mdp'];
        $newMdp = $_GET['mdp'];
        $newMdpVerif = $_GET['mdp2'];

        $user = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($newMdp != $newMdpVerif) {
            self::afficherErreur("Les mots de passe sont distincts !");
        } else if (!MotDePasse::verifier($currentMdp, $user->getMdpHache())) {
            self::afficherErreur("Le mot de passe rentré est erroné.");
        } else {
            $user->setPrenom($_GET['prenom']);
            $user->setNom($_GET['nom']);
            $user->setMdpHache(MotDePasse::hacher($newMdp));
            $user->setEstAdmin(isset($_GET['estAdmin']) && $_GET['estAdmin'] == 'on' && ConnexionUtilisateur::estAdministrateur());

            (new UtilisateurRepository())->mettreAJour($user);

            self::afficherVueDansCorps("Liste", 'utilisateur/utilisateurMisAJour.php', [
                'login' => $user->getLogin(),
                'utilisateurs' => (new UtilisateurRepository())->recuperer()
            ]);
        }
    }

    public static function supprimer(): void
    {
        if (isset($_GET['login']))
            $login = $_GET['login'];
        else {
            self::afficherErreur("Pas de login trouvé");
            return;
        }
        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            return;
        }

        (new UtilisateurRepository())->supprimer($login);
        self::afficherVueDansCorps("Liste", 'utilisateur/utilisateurSupprime.php', ['login' => $login, 'utilisateurs' => (new UtilisateurRepository())->recuperer()]);
    }


    public static function validerEmail()
    {
        if (!isset($_GET['login']) || !isset($_GET['nonce']))
            self::afficherErreur("Manque des données");
        else {
            $result = VerificationEmail::traiterEmailValidation($_GET['login'], $_GET['nonce']);
            if ($result)
                self::afficherDetail();
            else
                self::afficherErreur("Erreur dans la validation de l'e-mail.");
        }
    }
}
