<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private ?string $nom;
    private ?string $prenom;
    private string $mdpHache;
    private bool $estAdmin;

    private string $email;
    private string $emailAValider;
    private string $nonce;

    public function __construct(string $login, string $mdp, bool $estAdmin, string $email, string $emailAValider, string $nonce, string $nom = null, string $prenom = null)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdp;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

    public function formatTableau(): array
    {
        return array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
            "mdpHacheSalePoivreTag" => $this->mdpHache,
            "adminTag" => ($this->estAdmin ? 1 : 0),
            "emailTag" => $this->email,
            "emailAValiderTag" => $this->emailAValider,
            "nonceTag" => $this->nonce
        );
    }

    public function getEmail(): string
    {
        return $this->email;
    }
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }
    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }
    public function getNonce(): string
    {
        return $this->nonce;
    }
    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }

    public function getEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin($b): void
    {
        $this->estAdmin = $b;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

}