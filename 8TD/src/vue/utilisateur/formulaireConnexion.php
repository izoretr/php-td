<form method="GET">
    <fieldset>
        <legend style="font-size: 20px">Se connecter :</legend>
        <p>
            <label for="login_id">Login&#42; :</label>
            <input type="text" name="login" id="login_id" required/>
            <br/><br/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42; :</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="password" id="mdp_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
            <input type="hidden" name="controleur" value="utilisateur"/>
            <input type="hidden" name="action" value="connecter"/>
        </p>
    </fieldset>
</form>
