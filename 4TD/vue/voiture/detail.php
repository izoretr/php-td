<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Détails de la voiture <?php echo $voiture->getImmatriculation() ?></title>
    </head>
    <body>
        <p>La voiture
        <?php echo $voiture->getImmatriculation(); ?>
        a pour marque
        <?php echo $voiture->getMarque(); ?>
        .<br/>Elle est de couleur
        <?php echo $voiture->getCouleur(); ?>
        , et possède
        <?php echo $voiture->getNbSieges(); ?>
         sieges.
        </p>
    </body>
</html>
