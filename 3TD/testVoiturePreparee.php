<?php
    require_once "classes/Voiture.php";

    class MainClass {
        public static function testImmat() : void {
            $immat = "NOUVEAU";
            $voiture = Voiture::getVoitureParImmat($immat);
            if (isset($voiture)) {
                echo '<p>Voiture d\'immatriculation "' . $immat . '" : ' . $voiture . '</p>';
            }
            else
                echo '<p>Voiture non trouvée :(</br></p>';
        }
    }

    MainClass::testImmat();

    $nouvelleVoiture = new Voiture("NOUVEAU", "peugeot 404", "gris :(", 404);
    $nouvelleVoiture->sauvegarder();

    MainClass::testImmat();


