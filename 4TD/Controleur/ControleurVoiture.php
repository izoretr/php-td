<?php
require_once ('../Modele/ModeleVoiture.php'); // chargement du modèle
class ControleurVoiture {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
       extract($parametres); // Crée des variables à partir du tableau $parametres
       require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherListe() : void {
        $voitures = ModeleVoiture::getVoitures(); //appel au modèle pour gerer la BD
        self::afficherVue('../vue/voiture/liste.php', [
            'voitures' => $voitures
        ]);
    }
    public static function afficherDetail() : void {
        $immat = $_GET['immatriculation'];
        $voiture = ModeleVoiture::getVoitureParImmat($immat);

        if (isset($voiture))
            self::afficherVue('../vue/voiture/detail.php', [
                'voiture' => $voiture
            ]);
        else
            self::afficherVue('../vue/voiture/erreur.php');
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('../vue/voiture/formulaireCreation.html');
    }
    public static function creerDepuisFormulaire() : void {
        $tableauVoiture = [
            'immatriculation' => $_GET['immatriculation'],
            'couleur' => $_GET['couleur'],
            'marque' => $_GET['marque'],
            'nbSieges' => $_GET['nbPlaces']
        ];

        $voiture = ModeleVoiture::construireDepuisTableau($tableauVoiture);

        $voiture->sauvegarder();

        self::afficherListe();
    }
}
