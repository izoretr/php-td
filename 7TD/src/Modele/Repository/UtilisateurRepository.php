<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{
    public function construireDepuisTableau(array $objetFormatTableau) : Utilisateur {
        return new Utilisateur(
            $objetFormatTableau["login"],
            $objetFormatTableau["nom"],
            $objetFormatTableau["prenom"]
        );
    }


    protected function getNomTable(): string
    {
        return "utilisateur";
    }
    protected function getNomClePrimaire(): string
    {
        return "login";
    }
    protected function getNomsColonnes(): array
    {
        return array(
            'login',
            'nom',
            'prenom'
        );
    }
}
