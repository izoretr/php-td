<?php
    require_once "Trajet.php";
    require_once "Utilisateur.php";

    if (empty($_GET))
        echo "<p>Erreur 69 : Cette page doit être accédée à partir d'un formulaire.</p>";
    else {
        $trajetID = $_GET['trajetID'];

        echo "<ul>Passagers du trajet $trajetID :";
        foreach (Trajet::getPassagers($trajetID) as $passenger) {
            echo "<li>$passenger</li>";
        }
        echo '</ul><br/>';
    }
