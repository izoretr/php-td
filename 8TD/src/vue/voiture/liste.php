<h2>Voitures (<a href="?controleur=voiture&action=afficherFormulaireCreation">ajouter</a>) : </h2>
<ul>
    <?php
    foreach ($voitures as $voiture) {
        $immat = $voiture->getImmatriculation();

        echo '<li> Voiture d\'immatriculation '
            , '<a href="?controleur=voiture&action=afficherDetail&immatriculation='
            , rawurlencode($immat)
            , '">' . htmlspecialchars($immat) . '</a></li>'
        ;
    }
    ?>
</ul>
