<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\UtilisateurRepository as UtilisateurRepository;

class ControleurUtilisateur
{

    public static function afficherListe(): void
    {
        $users = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gerer la BD
        ControleurVues::afficherVueDansCorps('Liste d\'utilisateurs', 'utilisateur/liste.php', [
            'utilisateurs' => $users
        ]);
    }
    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $user = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (isset($user))
            ControleurVues::afficherVueDansCorps("Details", 'utilisateur/detail.php', [
                'user' => $user
            ]);
        else
            self::afficherErreur("Utilisateur non trouvé");
    }
    public static function afficherFormulaireCreation(): void
    {
        ControleurVues::afficherVueDansCorps("Formulaire de création", 'utilisateur/formulaireCreation.html');
    }
    public static function afficherFormulaireMiseAJour() : void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (isset($utilisateur))
            ControleurVues::afficherVueDansCorps("MàJ utilisateur", 'utilisateur/formulaireMiseAJour.php', [
                'login' => $utilisateur->getLogin(),
                'prenom' => $utilisateur->getPrenom(),
                'nom' => $utilisateur->getNom()
            ]);
        else
            self::afficherErreur("Utilisateur non trouvé");
    }
    public static function afficherErreur(string $error) : void {
        ControleurVues::afficherVueDansCorps("ERREUR", 'utilisateur/erreur.php', [
            'errorstr' => $error
        ]);
    }

    // ----- CRUD -----
    public static function creerDepuisFormulaire() : void
    {
        $tableauUtilisateur = [
            'login' => $_GET['login'],
            'nom' => $_GET['nom'],
            'prenom' => $_GET['prenom']
        ];

        $user = (new UtilisateurRepository())->construireDepuisTableau($tableauUtilisateur);

        (new UtilisateurRepository())->sauvegarder($user);

        ControleurVues::afficherVueDansCorps("Liste", 'utilisateur/utilisateurCree.php', [
            'utilisateurs' => (new UtilisateurRepository())->recuperer()
        ]);
    }
    public static function modifDepuisForm() : void {
        $tableauUtilisateur = [
            'login' => $_GET['login'],
            'prenom' => $_GET['prenom'],
            'nom' => $_GET['nom']
        ];

        $user = (new UtilisateurRepository())->construireDepuisTableau($tableauUtilisateur);

        (new UtilisateurRepository())->mettreAJour($user);

        ControleurVues::afficherVueDansCorps("Liste", 'utilisateur/utilisateurMisAJour.php', [
            'login' => $user->getLogin(),
            'utilisateurs' => (new UtilisateurRepository())->recuperer()
        ]);
    }
    public static function supprimer() : void {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            (new UtilisateurRepository())->supprimer($login);
            ControleurVues::afficherVueDansCorps("Liste", 'utilisateur/utilisateurSupprime.php', ['login' => $login, 'utilisateurs' => (new UtilisateurRepository())->recuperer()]);
        } else
            self::afficherErreur("Utilisateur à supprimer non-spécifié");
    }
}
