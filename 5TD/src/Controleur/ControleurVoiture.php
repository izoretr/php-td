<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\VoitureRepository as ModeleVoiture;

class ControleurVoiture
{
    private static function afficherVueCrue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/voiture/$cheminVue";
    }
    private static function afficherVueDansCorps(string $titrePage, string $cheminVue, array $parametres = []): void
    {
        self::afficherVueCrue("vueGenerale.php", array_merge(
            [
                'pageTitle' => $titrePage,
                'cheminVueBody' => $cheminVue
            ],
            $parametres
        ));
    }

    public static function afficherListe(): void
    {
        $voitures = VoitureRepository::getVoitures(); //appel au modèle pour gerer la BD
        self::afficherVueDansCorps('Liste de voitures', 'liste.php', [
            'voitures' => $voitures
        ]);
    }
    public static function afficherDetail(): void
    {
        $immat = $_GET['immatriculation'];
        $voiture = VoitureRepository::getVoitureParImmat($immat);

        if (isset($voiture))
            self::afficherVueDansCorps("Details", 'detail.php', [
                'voiture' => $voiture
            ]);
        else
            self::afficherVueDansCorps("ERREUR", 'erreur.php');
    }
    public static function afficherFormulaireCreation(): void
    {
        self::afficherVueDansCorps("Formulaire de création", 'formulaireCreation.html');
    }

    public static function creerDepuisFormulaire(): void
    {
        $tableauVoiture = [
            'immatriculation' => $_GET['immatriculation'],
            'couleur' => $_GET['couleur'],
            'marque' => $_GET['marque'],
            'nbSieges' => $_GET['nbPlaces']
        ];

        $voiture = VoitureRepository::construireDepuisTableau($tableauVoiture);

        $voiture->sauvegarder();

        self::afficherVueDansCorps("Voiture créée !", "voitureCreee.php", [
            'voitures' => $voitures = VoitureRepository::getVoitures()
        ]);
    }
}
