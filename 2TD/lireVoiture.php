<?php
    require_once "Model.php";
    require_once "classes/Voiture.php";

    $pdo = Model::getPdo();
    $pdoStatement = $pdo->query("SELECT * FROM voiture;");

    echo '<p>Voitures : <br/><br/>';
    foreach (Voiture::getVoitures() as $voiture) {
        echo $voiture . '<br/>';
    }
    echo '</p>';
